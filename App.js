import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';

import { RootStack } from './src/navigation';
import configureStore from './src/store';
import { mainTheme } from './src/core/styleGuide';

const App = () => (
  <Provider store={configureStore()}>
    <SafeAreaProvider>
      <ThemeProvider theme={mainTheme}>
        <NavigationContainer theme={mainTheme.navigationTheme}>
          <RootStack />
        </NavigationContainer>
      </ThemeProvider>
    </SafeAreaProvider>
  </Provider>
);

export default App;
