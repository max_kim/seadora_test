import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { HeaderButton } from '../../../presentations/Navigation/HeaderButton';
import { CartIcon } from '../../../presentations/icons/CartIcon';

import screens from '../../../navigation/screens';
import { getCartListAmount } from '../../../store/selectors/cart';

const mapStateToProps = (state) => ({
  cartAmount: getCartListAmount(state),
});

export const CartButton = connect(mapStateToProps)(
  React.memo(({ cartAmount, onPress }) => {
    return (
      <>
        <HeaderButton
          routePath={screens.productsStack.cartList}
          Icon={CartIcon}
          onPress={onPress}
        />
        {cartAmount ? <Marker /> : null}
      </>
    );
  }),
);

const Marker = styled.View`
  position: absolute;
  top: 28%;
  left: 60%;
  height: 6px;
  width: 6px;
  border-radius: 3px;
  background-color: ${({ theme }) => theme.secondary};
`;
