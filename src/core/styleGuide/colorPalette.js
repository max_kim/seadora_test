export const palette = Object.freeze({
  black: '#000000',
  midnightBlue: '#002B70',
  codGray: '#121212',
  quickSilver: '#A0A0A0',
  gallery: '#EEEEEE',
  culturedPearlLight: '#F5F5F5',
  culturedPearl: '#F7F7F7',
  internationalOrange: '#FF5000',
  white: '#FFFFFF',
});
