import { palette } from './colorPalette';

const navigationTheme = Object.freeze({
  dark: false,
  colors: {
    primary: palette.midnightBlue,
    background: palette.white,
    card: palette.white,
    notification: palette.internationalOrange,
    activeTintColor: palette.internationalOrange,
    inactiveTintColor: palette.quickSilver,
    text: palette.codGray,
    headerIcon: palette.white,
    border: palette.gallery,
  },
});

const indents = {
  container: {
    paddingHorizontal: 24,
  },
};

export const mainTheme = Object.freeze({
  primary: palette.midnightBlue,
  secondary: palette.internationalOrange,
  text: palette.codGray,
  inactive: palette.quickSilver,
  border: palette.gallery,
  background: palette.white,
  itemFooterBackground: palette.culturedPearlLight,
  cartFooterBackground: palette.culturedPearl,
  black: palette.black,
  white: palette.white,
  navigationTheme,
  indents,
});
