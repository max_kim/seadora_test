import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { ProductsStack } from './Products';

import screens from './screens';
import { withoutHeader } from './options';

const Stack = createStackNavigator();

export const RootStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        ...withoutHeader,
      }}>
      <Stack.Screen
        name={screens.root.productsStack}
        component={ProductsStack}
      />
    </Stack.Navigator>
  );
};
