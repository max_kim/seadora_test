export default Object.freeze({
  root: {
    productsStack: 'ProductsStack',
  },
  productsStack: {
    productsList: 'ProductsList',
    productInfo: 'ProductInfo',
    cartList: 'CartList',
  },
});
