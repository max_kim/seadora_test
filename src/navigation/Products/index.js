import React, { useMemo, useCallback } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { withTheme } from 'styled-components';

import { ProductsListScreen } from '../../screens/Products/ProductsList';
import { ProductInfoScreen } from '../../screens/Products/ProductInfo';
import { HeaderButton } from '../../presentations/Navigation/HeaderButton';
import { SeadoraLogo } from '../../presentations/icons/SeadoraLogo';
import { LeftArrow } from '../../presentations/icons/LeftArrow';

import screens from '../screens';
import { withHeader } from '../options';
import { CartButton } from '../../containers/Cart/CartButton';
import { CartScreen } from '../../screens/Cart';

const Stack = createStackNavigator();

export const ProductsStack = ({ navigation }) => {
  const headerTitle = useMemo((props) => <SeadoraLogo {...props} />, []);

  const headerRight = useCallback(() => <CartButton />, []);

  const goBack = useCallback(() => navigation.goBack(), [navigation]);

  const headerLeft = useCallback(
    () => (
      <HeaderButton
        onPress={goBack}
        Icon={withTheme(({ theme }) => (
          <LeftArrow color={theme.white} />
        ))}
      />
    ),
    [goBack],
  );

  const screenOptions = useMemo(
    () => withHeader({ headerTitle, headerRight }),
    [headerRight, headerTitle],
  );

  const cartHeaderRight = useCallback(() => <CartButton onPress={goBack} />, [
    goBack,
  ]);

  return (
    <Stack.Navigator screenOptions={screenOptions}>
      <Stack.Screen
        name={screens.productsStack.productsList}
        component={ProductsListScreen}
      />
      <Stack.Screen
        name={screens.productsStack.productInfo}
        component={ProductInfoScreen}
        options={{ headerLeft }}
      />
      <Stack.Screen
        name={screens.productsStack.cartList}
        component={CartScreen}
        options={{ headerRight: cartHeaderRight }}
      />
    </Stack.Navigator>
  );
};
