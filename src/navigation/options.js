import { mainTheme } from '../core/styleGuide';

export const headerStyle = {
  backgroundColor: mainTheme.primary,
  borderBottomWidth: 0,
  elevation: 0,
};

export const withHeader = ({ headerTitle, headerRight }) => ({
  headerShown: true,
  headerMode: 'screen',
  headerLeft: null,
  headerTitle,
  headerRight,
  headerStyle,
  headerTitleAlign: 'center',
});

export const withoutHeader = {
  headerShown: false,
  headerMode: 'none',
};
