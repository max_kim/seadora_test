import React, { useMemo, useCallback } from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { CartScreen } from '../../screens/Cart';
import { SeadoraLogo } from '../../presentations/icons/SeadoraLogo';

import screens from '../screens';
import { withHeader } from '../options';
import { CartButton } from '../../containers/Cart/CartButton';

const Stack = createStackNavigator();

export const CartStack = ({ navigation }) => {
  const headerTitle = useMemo((props) => <SeadoraLogo {...props} />, []);

  const goBack = useCallback(() => navigation.goBack(), [navigation]);

  const headerRight = useCallback(() => <CartButton onPress={goBack} />, [
    goBack,
  ]);

  const screenOptions = useMemo(
    () => withHeader({ headerTitle, headerRight }),
    [headerRight, headerTitle],
  );

  return (
    <Stack.Navigator screenOptions={screenOptions}>
      <Stack.Screen
        name={screens.productsStack.cartList}
        component={CartScreen}
      />
    </Stack.Navigator>
  );
};
