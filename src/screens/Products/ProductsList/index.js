import React, { useState, useEffect, useCallback, useMemo } from 'react';
import { FlatList, Dimensions } from 'react-native';
import styled from 'styled-components';
import { connect, useDispatch } from 'react-redux';

import Container from '../../../presentations/common/Container';
import { ProductsListItem } from '../../../presentations/Products/ProductsList/ProductsListItem';
import { Button, buttonTypes } from '../../../presentations/common/Button';

import { getProductsList } from '../../../store/selectors/products';
import { productsAsyncActions } from '../../../store/redux/products/actions';
import screens from '../../../navigation/screens';
import { errorAlert } from '../../../core/utils';

const { width: screenWidth } = Dimensions.get('screen');

const ProductsList = ({ navigation, productsList }) => {
  const dispatch = useDispatch();

  const [nextPage, setNextPage] = useState(null);
  const [refreshing, setRefreshing] = useState(true);

  const fetchProducts = useCallback(
    (payload) => dispatch(productsAsyncActions.fetchProductsList(payload)),
    [dispatch],
  );

  const resetRefreshing = useCallback(() => setRefreshing(false), []);

  const onError = useCallback(
    (message) => {
      errorAlert({ message });
      resetRefreshing();
    },
    [resetRefreshing],
  );

  const initialFetchProducts = useCallback(() => {
    fetchProducts({
      page: 1,
      setNextPage: (next) => {
        setNextPage(next);
        setRefreshing(false);
      },
      onSuccess: resetRefreshing,
      onError,
    });
  }, [fetchProducts, onError, resetRefreshing]);

  const fetchNextPage = useCallback(() => {
    nextPage && fetchProducts({ page: nextPage, setNextPage });
  }, [fetchProducts, nextPage]);

  useEffect(initialFetchProducts, []);

  const onPressItem = useCallback(
    (id) => {
      navigation.navigate(screens.productsStack.productInfo, { id });
    },
    [navigation],
  );

  const ListFooterComponent = useCallback(
    () =>
      nextPage ? (
        <BottomButtonBox>
          <BottomButton
            type={buttonTypes.SECONDARY}
            title={'Показать еще'}
            onPress={fetchNextPage}
            disabled={false}
          />
        </BottomButtonBox>
      ) : null,
    [fetchNextPage, nextPage],
  );

  const itemHeight = useMemo(() => ((screenWidth - 48) / 312) * 208 + 155, []);

  return (
    <Container>
      <TitleBox>
        <TitleText>{'Каталог'}</TitleText>
      </TitleBox>
      <ListBox>
        <StyledList
          data={productsList}
          renderItem={({ item }) => (
            <ProductsListItem
              style={{ height: itemHeight }}
              item={item}
              onPress={onPressItem}
            />
          )}
          keyExtractor={({ id }) => String(id)}
          ItemSeparatorComponent={ItemSeparatorComponent}
          ListFooterComponent={ListFooterComponent}
          refreshing={refreshing}
          onRefresh={initialFetchProducts}
          showsVerticalScrollIndicator={false}
        />
      </ListBox>
    </Container>
  );
};

const TitleBox = styled.View`
  height: 64px;
  width: 100%;
  padding-top: 25px;
`;

const TitleText = styled.Text`
  font-size: 24px;
  font-weight: bold;
`;

const ListBox = styled.View`
  flex: 1;
  width: 100%;
`;

const StyledList = styled(FlatList).attrs(
  ({ data, contentContainerStyle }) => ({
    contentContainerStyle: {
      flex: Number(!data?.length),
      ...contentContainerStyle,
    },
  }),
)`
  width: 100%;
`;

const BottomButtonBox = styled.View`
  align-items: center;
  height: 96px;
  width: 100%;
  padding-top: 24px;
  padding-bottom: 32px;
`;

const BottomButton = styled(Button)`
  width: 70%;
  min-height: 40px;
`;

const ItemSeparatorComponent = styled.View`
  height: 24px;
`;

const mapStateToProps = (state) => ({
  productsList: getProductsList(state),
});

export const ProductsListScreen = connect(mapStateToProps)(ProductsList);
