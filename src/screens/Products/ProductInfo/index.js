import React, { useState, useCallback, useEffect, useMemo } from 'react';
import styled from 'styled-components';
import { connect, useDispatch } from 'react-redux';

import Container from '../../../presentations/common/Container';
import { FastImage } from '../../../presentations/common/FastImageWithPreloader';
import { Button } from '../../../presentations/common/Button';
import { Spinner } from '../../../presentations/common/Spinner';

import { productsAsyncActions } from '../../../store/redux/products/actions';
import { cartAsyncActions } from '../../../store/redux/cart/actions';
import { getCurrentProduct } from '../../../store/selectors/products';
import { isAlreadyAddedToCart } from '../../../store/selectors/cart';
import { errorAlert } from '../../../core/utils';
import { Info } from '../../../presentations/icons/Info';
import screens from '../../../navigation/screens';

const ProductInfo = ({ current, isInTheCart, navigation, route }) => {
  const dispatch = useDispatch();

  const { id, img, name, price100, weight, isAvailable } = current || {};

  const totalAmount = useMemo(() => (price100 * weight) / 100, [
    price100,
    weight,
  ]);
  const titleName = useMemo(() => name && name.replace(/\d+.*$/g, ''), [name]);
  const titleWeight = useMemo(() => name && name.replace(titleName, ''), [
    name,
    titleName,
  ]);

  const currentId = route.params?.id;

  const [isUploading, setIsUploading] = useState(true);

  const onSuccess = useCallback(() => {
    setIsUploading(false);
  }, []);

  const onError = useCallback((message) => {
    errorAlert({ message });
  }, []);

  const fetchCurrentProduct = useCallback(() => {
    dispatch(
      productsAsyncActions.fetchProduct({ id: currentId, onSuccess, onError }),
    );
  }, [currentId, dispatch, onError, onSuccess]);

  const onPressItem = useCallback(
    () =>
      dispatch(
        cartAsyncActions.addToCart({
          product: {
            id,
            name: titleName,
            weight,
            price100,
            totalAmount,
            img,
          },
          onSuccess: () => navigation.navigate(screens.productsStack.cartList),
        }),
      ),
    [dispatch, id, titleName, weight, price100, totalAmount, img, navigation],
  );

  const contentContainerStyle = useMemo(
    () => ({
      justifyContent: 'space-between',
      alignItems: 'center',
      paddingTop: 24,
      paddingHorizontal: 24,
    }),
    [],
  );

  useEffect(fetchCurrentProduct, []);

  return (
    <ContainerWrapper>
      {isUploading ? (
        <Spinner />
      ) : (
        <>
          <ImageBox>
            <FastImage uri={img} />
          </ImageBox>

          <ContentBlock contentContainerStyle={contentContainerStyle}>
            <InfoBlock>
              <TitleText>{titleName}</TitleText>
              <TitleText>{titleWeight}</TitleText>

              <PriceBlock>
                <PriceText>{'Стоимость'}</PriceText>
                <PriceLeftBlock>
                  <PriceTextBold>{`${price100} грн`}</PriceTextBold>
                  <PriceText>{' / 100 г'}</PriceText>
                </PriceLeftBlock>
              </PriceBlock>

              <OrderBlock>
                <OrderText>К предзаказу:</OrderText>
                <OrderText>{`${weight} г`}</OrderText>
              </OrderBlock>
            </InfoBlock>

            <TotalPriceBlock>
              <TotalPriceAmount>{`~ ${totalAmount}`}</TotalPriceAmount>
              <TotalPriceCurrency>{' грн'}</TotalPriceCurrency>
            </TotalPriceBlock>

            <ButtonBlock>
              {isAvailable ? (
                <BottomButton
                  title={isInTheCart ? 'Товар в корзине' : 'Добавить в корзину'}
                  onPress={onPressItem}
                  disabled={isInTheCart}
                />
              ) : (
                <UnavailableBlock>
                  <Info />
                  <UnavailableText>
                    Товар не доступен к предзаказу
                  </UnavailableText>
                </UnavailableBlock>
              )}
            </ButtonBlock>
          </ContentBlock>
        </>
      )}
    </ContainerWrapper>
  );
};

const ContainerWrapper = styled(Container)`
  padding: 0 -${({ theme }) => theme.indents.container.paddingHorizontal}px;
`;

const ImageBox = styled.View`
  flex: 1;
  width: 100%;
`;

const ContentBlock = styled.ScrollView`
  flex: 1;
  width: 100%;
`;

const InfoBlock = styled.View`
  width: 100%;
`;

const PriceBlock = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding-top: 22px;
`;

const PriceLeftBlock = styled.View`
  flex-direction: row;
`;

const OrderBlock = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding-top: 52px;
`;

const TotalPriceBlock = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
  width: 100%;
  padding-top: 14px;
`;

const ButtonBlock = styled.View`
  height: 112px;
  width: 100%;
  padding: 32px 0;
`;

const UnavailableBlock = styled.View`
  flex-direction: row;
  width: 100%;
`;

const TitleText = styled.Text`
  font-size: 24px;
  font-weight: bold;
  color: ${({ theme }) => theme.text};
`;

const PriceText = styled.Text`
  font-size: 18px;
`;

const PriceTextBold = styled(PriceText)`
  font-weight: bold;
`;

const OrderText = styled(PriceTextBold)``;

const TotalPriceAmount = styled(PriceTextBold)`
  font-size: 32px;
  line-height: 32px;
`;

const TotalPriceCurrency = styled(PriceTextBold)``;

const UnavailableText = styled.Text`
  font-size: 12px;
  margin-left: 16px;
`;

const BottomButton = styled(Button)`
  min-height: 48px;
  width: 100%;
`;

const mapStateToProps = (state) => {
  const current = getCurrentProduct(state);

  return {
    current,
    isInTheCart: isAlreadyAddedToCart(state, current?.id),
  };
};

export const ProductInfoScreen = connect(mapStateToProps)(ProductInfo);
