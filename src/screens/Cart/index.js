import React, { useCallback, useMemo } from 'react';
import { FlatList } from 'react-native';
import styled from 'styled-components';
import { connect, useDispatch } from 'react-redux';

import Container from '../../presentations/common/Container';
import { CartItem } from '../../presentations/Cart/CartItem';
import { HeaderButton } from '../../presentations/Navigation/HeaderButton';
import { LeftArrow } from '../../presentations/icons/LeftArrow';

import { cartAsyncActions } from '../../store/redux/cart/actions';
import { getCartList, getCartListAmount } from '../../store/selectors/cart';

const CartList = ({ navigation, cartList, cartAmount }) => {
  const dispatch = useDispatch();

  const goBack = useCallback(() => navigation.goBack(), [navigation]);

  const onPressRemove = useCallback(
    (id) => {
      dispatch(cartAsyncActions.removeFromCart({ id }));
    },
    [dispatch],
  );

  const contentContainerStyle = useMemo(() => ({ paddingHorizontal: 24 }), []);

  return (
    <ContainerWrapper>
      <TitleBox>
        <BackButton onPress={goBack} Icon={() => <LeftArrow />} />
        <TitleText>{'Корзина'}</TitleText>
      </TitleBox>
      <ListBox>
        <StyledList
          data={cartList}
          renderItem={({ item }) => (
            <CartItem item={item} onPress={onPressRemove} />
          )}
          keyExtractor={({ id }) => String(id)}
          ItemSeparatorComponent={ItemSeparatorComponent}
          contentContainerStyle={contentContainerStyle}
          showsVerticalScrollIndicator={false}
        />
      </ListBox>
      <FooterBlock>
        <FooterText>Общая стоимость</FooterText>
        <FooterText>{`${cartAmount} грн`}</FooterText>
      </FooterBlock>
    </ContainerWrapper>
  );
};

const ContainerWrapper = styled(Container)`
  padding: 0;
`;

const TitleBox = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  height: 64px;
  width: 100%;
  padding-top: 25px;
`;

const BackButton = styled(HeaderButton)`
  position: absolute;
  top: 37px;
  left: 0;
  padding-left: 24px;
`;

const TitleText = styled.Text`
  font-size: 24px;
  font-weight: bold;
`;

const ListBox = styled.View`
  flex: 1;
  width: 100%;
`;

const StyledList = styled(FlatList).attrs(
  ({ data, contentContainerStyle }) => ({
    contentContainerStyle: {
      flex: Number(!data?.length),
      ...contentContainerStyle,
    },
  }),
)`
  width: 100%;
`;

const ItemSeparatorComponent = styled.View`
  height: 1px;
  background-color: ${({ theme }) => theme.border};
`;

const FooterBlock = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  height: 64px;
  width: 100%;
  padding: 0 24px;
  background-color: ${({ theme }) => theme.cartFooterBackground};
`;

const FooterText = styled.Text`
  font-size: 16px;
  font-weight: bold;
  color: ${({ theme }) => theme.text};
`;

const mapStateToProps = (state) => ({
  cartList: getCartList(state),
  cartAmount: getCartListAmount(state),
});

export const CartScreen = connect(mapStateToProps)(CartList);
