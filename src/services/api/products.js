import client from './index';

export const productsApi = {
  getProductsList: (page) => client.get(`/products?page=${page}`),
  getProduct: (id) => client.get(`/products/${id}`),
};
