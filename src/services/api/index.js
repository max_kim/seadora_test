import axios from 'axios';

import { API_ROOT, TOKEN } from '../../core/configs/constants';

const client = axios.create({
  baseURL: API_ROOT,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: TOKEN,
  },
  timeout: 1500,
});

const errorInterceptor = (error) => {
  let message = 'Что-то пошло не так.';

  if (error.response) {
    if (error.response.data?.error) {
      message = error.response.data.error;
    }
  } else if (error.request) {
    message = 'Сервер не отвечает.';
  } else {
    message = error.message;
  }
  console.warn('api error', error.config);

  return Promise.reject({ message });
};

client.interceptors.response.use((response) => response.data, errorInterceptor);

export default client;
