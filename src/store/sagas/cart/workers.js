import { call, put, select } from 'redux-saga/effects';
import _ from 'lodash';

import { getCartList, getCartListWithoutId } from '../../selectors/cart';
import { cartActions } from '../../redux/cart/actions';

export function* addToCart({ payload: { product, onSuccess, onError } }) {
  try {
    const cartList = yield select(getCartList);

    const updated = _.unionBy(cartList, [product], 'id');

    yield put(cartActions.setCartList(updated));

    onSuccess && (yield call(onSuccess));
  } catch (e) {
    onError && (yield call(onError, e.message));
  }
}

export function* removeFromCart({ payload: { id, onSuccess, onError } }) {
  try {
    const updated = yield select((state) => getCartListWithoutId(state, id));

    yield put(cartActions.setCartList(updated));

    onSuccess && (yield call(onSuccess));
  } catch (e) {
    onError && (yield call(onError, e.message));
  }
}
