import { takeLatest } from 'redux-saga/effects';

import { asyncTypes } from '../../redux/cart/types';
import { addToCart, removeFromCart } from './workers';

export const cartSagas = [
  takeLatest(asyncTypes.ADD_PRODUCT_TO_CART, addToCart),
  takeLatest(asyncTypes.REMOVE_PRODUCT_FROM_CART, removeFromCart),
];
