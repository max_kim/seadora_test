import { takeLatest } from 'redux-saga/effects';

import { asyncTypes } from '../../redux/products/types';
import { fetchProductsList, fetchProduct } from './workers';

export const productsSagas = [
  takeLatest(asyncTypes.FETCH_PRODUCTS_LIST, fetchProductsList),
  takeLatest(asyncTypes.FETCH_PRODUCT, fetchProduct),
];
