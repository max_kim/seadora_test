import { call, put, select } from 'redux-saga/effects';
import _ from 'lodash';

import { productsApi } from '../../../services/api/products';
import { productsActions } from '../../redux/products/actions';
import { getProductsList } from '../../selectors/products';

export function* fetchProductsList({
  payload: { page, setNextPage, onSuccess, onError },
}) {
  try {
    const {
      data: { items, last_page },
    } = yield call([productsApi, 'getProductsList'], page);

    const list = yield select(getProductsList);
    const updated = page && page > 1 ? _.unionBy(list, items, 'id') : items;

    yield put(productsActions.setProductsList(updated));

    yield call(setNextPage, page < last_page ? ++page : null);

    onSuccess && (yield call(onSuccess));
  } catch (e) {
    onError && (yield call(onError, e.message));
  }
}

export function* fetchProduct({ payload: { id, onSuccess, onError } }) {
  try {
    const { data } = yield call([productsApi, 'getProduct'], id);

    yield put(productsActions.setCurrentProduct(data));

    yield call(onSuccess);
  } catch (e) {
    yield call(onError, e.message);
  }
}
