import { all } from 'redux-saga/effects';

import { productsSagas } from './products';
import { cartSagas } from './cart';

export default function* root() {
  yield all([...productsSagas, ...cartSagas]);
}
