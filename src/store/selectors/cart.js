import { createSelector } from 'reselect';
import { createCachedSelector } from 're-reselect';

const cartListSelector = (state) => state.cart.list;

export const getCartList = createSelector(
  [cartListSelector],
  (cartList) => cartList,
);

export const getCartListAmount = createSelector(
  [cartListSelector],
  (cartList) =>
    cartList.reduce((accum, { totalAmount }) => accum + totalAmount, 0),
);

export const getCartListWithoutId = createCachedSelector(
  [cartListSelector, (state, id) => id],
  (cartList, id) => cartList.filter((item) => item.id !== id),
)((state, id) => id);

export const isAlreadyAddedToCart = createCachedSelector(
  [cartListSelector, (state, id) => id],
  (cartList, id) => !!cartList.find((item) => item.id === id),
)((state, id) => id);
