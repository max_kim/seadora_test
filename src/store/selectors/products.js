import { createSelector } from 'reselect';
import { createCachedSelector } from 're-reselect';

const productsListSelector = (store) => store.products.list;

const currentProductSelector = (store) => store.products.current;

export const getProductsList = createSelector(
  [productsListSelector],
  (productsList) => productsList,
);

export const getCurrentProduct = createSelector(
  [currentProductSelector],
  (currentProduct) => currentProduct,
);

export const getProductById = createCachedSelector(
  [productsListSelector, (state, id) => id],
  (productsList, productId) =>
    productsList.find((item) => (item.id = productId)),
)((state, id) => id);
