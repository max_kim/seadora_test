export const types = Object.freeze({
  SET_CART_LIST: 'SET_CART_LIST',
});

export const asyncTypes = Object.freeze({
  ADD_PRODUCT_TO_CART: 'ADD_PRODUCT_TO_CART',
  REMOVE_PRODUCT_FROM_CART: 'REMOVE_PRODUCT_FROM_CART',
});
