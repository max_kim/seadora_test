import { types } from './types';

const initialState = {
  list: [],
};

export const cart = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_CART_LIST:
      return {
        ...state,
        list: action.payload,
      };

    default:
      return state;
  }
};
