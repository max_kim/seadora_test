import { types, asyncTypes } from './types';

export const cartActions = Object.freeze({
  setCartList: (payload) => ({
    type: types.SET_CART_LIST,
    payload,
  }),
});

export const cartAsyncActions = Object.freeze({
  addToCart: (payload) => ({
    type: asyncTypes.ADD_PRODUCT_TO_CART,
    payload,
  }),
  removeFromCart: (payload) => ({
    type: asyncTypes.REMOVE_PRODUCT_FROM_CART,
    payload,
  }),
});
