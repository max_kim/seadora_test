import { combineReducers } from 'redux';

import { products } from './products/reducer';
import { cart } from './cart/reducers';

export default combineReducers({
  products,
  cart,
});
