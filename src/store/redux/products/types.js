export const types = Object.freeze({
  SET_PRODUCTS_LIST: 'SET_PRODUCTS_LIST',
  SET_CURRENT_PRODUCT: 'SET_CURRENT_PRODUCT',
});

export const asyncTypes = Object.freeze({
  FETCH_PRODUCTS_LIST: 'FETCH_PRODUCTS_LIST',
  FETCH_PRODUCT: 'FETCH_PRODUCT',
});
