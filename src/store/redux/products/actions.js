import { types, asyncTypes } from './types';

export const productsActions = Object.freeze({
  setProductsList: (payload) => ({
    type: types.SET_PRODUCTS_LIST,
    payload,
  }),
  setCurrentProduct: (payload) => ({
    type: types.SET_CURRENT_PRODUCT,
    payload,
  }),
});

export const productsAsyncActions = Object.freeze({
  fetchProductsList: (payload) => ({
    type: asyncTypes.FETCH_PRODUCTS_LIST,
    payload,
  }),
  fetchProduct: (payload) => ({
    type: asyncTypes.FETCH_PRODUCT,
    payload,
  }),
});
