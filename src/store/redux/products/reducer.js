import { types } from './types';

const initialState = {
  list: [],
  current: null,
};

export const products = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_PRODUCTS_LIST:
      return {
        ...state,
        list: action.payload,
      };

    case types.SET_CURRENT_PRODUCT:
      return {
        ...state,
        current: action.payload,
      };

    default:
      return state;
  }
};
