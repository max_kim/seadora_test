import React from 'react';
import Svg, { Path } from 'react-native-svg';
import { withTheme } from 'styled-components';

export const LeftArrow = withTheme(({ theme, color, ...props }) => {
  return (
    <Svg width={16} height={14} viewBox="0 0 16 14" fill="none" {...props}>
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M3.812 6.01L8.448 1.4 7.04 0 1.408 5.6 0 7l7.04 7 1.408-1.4-4.636-4.61H16V6.01H3.812z"
        fill={color || theme.primary}
      />
    </Svg>
  );
});
