import React from 'react';
import Svg, { Path } from 'react-native-svg';
import { withTheme } from 'styled-components';

export const Close = withTheme(({ theme, ...props }) => {
  return (
    <Svg width={16} height={16} viewBox="0 0 16 16" fill="none" {...props}>
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M.195 15.805a.667.667 0 010-.943L7.057 8 .195 1.138a.667.667 0 11.943-.943L8 7.057 14.862.195a.667.667 0 11.943.943L8.943 8l6.862 6.862a.667.667 0 11-.943.943L8 8.943l-6.862 6.862a.667.667 0 01-.943 0z"
        fill={theme.inactive}
      />
    </Svg>
  );
});
