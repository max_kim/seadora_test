import React, { useCallback } from 'react';
import { useNavigation } from '@react-navigation/native';
import styled from 'styled-components';

export const HeaderButton = React.memo(
  ({ onPress, label, Icon, routePath, color, style, textStyle }) => {
    const { navigate } = useNavigation();

    const navigateTo = useCallback(() => {
      routePath && navigate(routePath);
    }, [navigate, routePath]);

    return (
      <ButtonWrapper onPress={onPress || navigateTo} style={style}>
        {Icon && <Icon />}
        {label ? (
          <ButtonText color={color} style={textStyle}>
            {label}
          </ButtonText>
        ) : null}
      </ButtonWrapper>
    );
  },
);

const ButtonWrapper = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  padding-horizontal: ${({ theme }) =>
    theme.indents.container.paddingHorizontal}px;
`;

const ButtonText = styled.Text`
  font-size: 16px;
  color: ${({ theme }) => theme.white};
`;
