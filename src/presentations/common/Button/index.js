import React, { useMemo } from 'react';
import { View, TouchableOpacity } from 'react-native';
import styled, { withTheme } from 'styled-components';

export const buttonTypes = Object.freeze({
  PRIMARY: 'PRIMARY',
  SECONDARY: 'SECONDARY',
});

export const Button = withTheme(
  React.memo(
    ({
      style,
      marginLeft,
      marginRight,
      titleStyle = {},
      title,
      type,
      disabled,
      onPress,
      onPressValue,
      theme,
    }) => {
      const buttonStyle = useMemo(() => {
        switch (type) {
          case buttonTypes.SECONDARY:
            return {
              backgroundColor: theme.white,
              borderWidth: 1,
              borderColor: theme.primary,
            };
          default:
            return {
              backgroundColor: disabled ? theme.inactive : theme.secondary,
            };
        }
      }, [theme, type, disabled]);

      const textStyle = useMemo(() => {
        switch (type) {
          case buttonTypes.SECONDARY:
            return { color: theme.primary };
          default:
            return { color: theme.white };
        }
      }, [theme, type]);

      return (
        <ButtonBlock
          as={!onPress ? View : TouchableOpacity}
          style={[buttonStyle, style]}
          marginLeft={marginLeft}
          marginRight={marginRight}
          disabled={disabled}
          onPress={onPress ? () => onPress(onPressValue) : null}>
          <TitleText style={[textStyle, titleStyle]} numberOfLines={1}>
            {title}
          </TitleText>
        </ButtonBlock>
      );
    },
  ),
);

export const ButtonBlock = styled.TouchableOpacity`
  flex: 1;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  height: 40px;
  margin-left: ${({ marginLeft, theme }) =>
    marginLeft ? theme.indents.container.paddingHorizontal : 0}px;
  margin-right: ${({ marginRight, theme }) =>
    marginRight ? theme.indents.container.paddingHorizontal : 0}px;
  padding: 12px;
  border-radius: 8px;
`;

export const TitleText = styled.Text`
  font-size: 16px;
`;
