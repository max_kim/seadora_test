import React from 'react';
import SafeAreaView from 'react-native-safe-area-view';
import styled from 'styled-components';

const Container = ({ style, children }) => {
  return <WrappedSafeAreaView style={style}>{children}</WrappedSafeAreaView>;
};

const WrappedSafeAreaView = styled(SafeAreaView)`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: 0 ${({ theme }) => theme.indents.container.paddingHorizontal}px;
  background-color: ${({ theme }) => theme.background};
`;

export default Container;
