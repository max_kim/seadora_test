import React, { useState, useMemo } from 'react';
import styled from 'styled-components';
import FastImageLib from 'react-native-fast-image';

import { Spinner } from '../Spinner';

export const FastImage = React.memo((props) => {
  const {
    containerStyle,
    showPreloader = false,
    preloaderStyle,
    style,
    uri,
    source,
    onLoadStart,
    onLoadEnd,
    onError,
    ...others
  } = props;

  const [preloader, setPreloader] = useState(showPreloader);

  const fastStyle = useMemo(() => ({ flex: 1 }), []);

  return (
    <Container style={containerStyle}>
      <FastImageLib
        style={[fastStyle, style]}
        source={{ uri, source }}
        onLoadStart={() => {
          setPreloader(true);
        }}
        onLoadEnd={() => {
          setPreloader(false);
        }}
        onError={() => {
          setPreloader(false);
        }}
        {...others}
      />
      {preloader && <Spinner />}
    </Container>
  );
});

const Container = styled.View`
  flex: 1;
`;
