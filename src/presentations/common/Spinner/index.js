import React from 'react';
import { ActivityIndicator, StyleSheet } from 'react-native';
import styled from 'styled-components';

import { palette } from '../../../core/styleGuide';

export const Spinner = React.memo(() => {
  return (
    <SpinnerBox style={StyleSheet.absoluteFill}>
      <ActivityIndicator size={'large'} color={palette.midnightBlue} />
    </SpinnerBox>
  );
});

const SpinnerBox = styled.View`
  justify-content: center;
  align-items: center;
`;
