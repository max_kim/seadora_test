import React, { useCallback } from 'react';
import styled from 'styled-components';

import { FastImage } from '../../common/FastImageWithPreloader';
import { Close } from '../../icons/Close';

export const CartItem = React.memo(
  ({ item: { id, name, totalAmount, img, weight }, onPress, style }) => {
    const onPressItem = useCallback(() => onPress(id), [id, onPress]);

    return (
      <ListItemBlock style={style}>
        <TopItemBlock>
          <ImageBox>
            <FastImage uri={img} />
          </ImageBox>
          <NameBox>
            <NameText>{name}</NameText>
          </NameBox>
          <DeleteButton onPress={onPressItem}>
            <Close />
          </DeleteButton>
        </TopItemBlock>

        <ContentBlock>
          <WeightBlock>
            <SimpleText>{'Вес'}</SimpleText>
            <SimpleText>{`${weight} г`}</SimpleText>
          </WeightBlock>
          <PriceBlock>
            <SimpleText>{'Сумма'}</SimpleText>
            <SimpleText>{`${totalAmount} грн`}</SimpleText>
          </PriceBlock>
        </ContentBlock>
      </ListItemBlock>
    );
  },
);

const ListItemBlock = styled.View`
  justify-content: center;
  width: 100%;
  padding: 20px 0;
`;

const TopItemBlock = styled.View`
  flex-direction: row;
  height: 48px;
  width: 100%;
`;

const ImageBox = styled.View`
  height: 48px;
  width: 72px;
  margin-right: 16px;
  border-radius: 8px;
  overflow: hidden;
`;

const NameBox = styled.View`
  flex: 1;
  width: 100%;
`;

const DeleteButton = styled.TouchableOpacity`
  padding-left: 16px;
`;

const ContentBlock = styled.View`
  padding-left: 88px;
`;

const WeightBlock = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-top: 9px;
`;

const PriceBlock = styled(WeightBlock)``;

const NameText = styled.Text`
  font-size: 12px;
  font-weight: bold;
  color: ${({ theme }) => theme.text};
`;

const SimpleText = styled(NameText)`
  font-weight: normal;
`;
