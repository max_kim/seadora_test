import React, { useCallback, useMemo } from 'react';
import styled from 'styled-components';

import { FastImage } from '../../../common/FastImageWithPreloader';
import { Button } from '../../../common/Button';

export const ProductsListItem = React.memo(
  ({ item: { id, name, price100, img }, onPress, style }) => {
    const titleName = useMemo(() => name.replace(/\d+.*$/g, ''), [name]);
    const titleWeight = useMemo(() => name.replace(titleName, ''), [
      name,
      titleName,
    ]);

    const onPressItem = useCallback(() => onPress(id), [id, onPress]);

    return (
      <ListItemBlock style={style}>
        <ImageBox>
          <FastImage uri={img} />
        </ImageBox>

        <ContentBlock>
          <ItemText>{titleName}</ItemText>
          <ItemText>{titleWeight}</ItemText>
          <PriceBlock>
            <PriceTextBold>{price100}</PriceTextBold>
            <CurrencyTextBold>{' грн '}</CurrencyTextBold>
            <ItemText>{'(100 г)'}</ItemText>
          </PriceBlock>
          <BottomButton title={'Заказать'} onPress={onPressItem} />
        </ContentBlock>
      </ListItemBlock>
    );
  },
);

const ListItemBlock = styled.View`
  width: 100%;
  padding-bottom: 16px;
  justify-content: center;
  border: solid 1px ${({ theme }) => theme.border};
  border-radius: 8px;
  overflow: hidden;
`;

const ImageBox = styled.View`
  flex: 1;
  width: 100%;
`;

const ContentBlock = styled.View`
  justify-content: center;
  align-items: center;
  height: 155px;
  width: 100%;
  margin-top: 19px;
  padding-horizontal: 24px;
`;

const ItemText = styled.Text`
  font-size: 18px;
  color: ${({ theme }) => theme.text};
`;

const CurrencyTextBold = styled(ItemText)`
  font-weight: bold;
`;

const PriceTextBold = styled(CurrencyTextBold)`
  font-size: 24px;
`;

const PriceBlock = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-top: 15px;
`;

const BottomButton = styled(Button)`
  min-height: 40px;
  width: 100%;
  margin-top: 19px;
  margin-bottom: 25px;
`;
